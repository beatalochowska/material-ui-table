import { fakeData } from "../TableData/TableData";

export const fetchFakeData = () => {
    return new Promise<string>((resolve, reject) => {
        setTimeout(() => resolve(fakeData), 2000);
        // setTimeout(() => reject("500, błąd serwera"), 500);
    });
}