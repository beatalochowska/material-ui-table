export const sortFunc = <T>(a: T, b: T, by: keyof T) => {
    const x = a;
    const y = a[by];
};

interface A {
    x: number;
    y: number;
}

const a: A = {
    x: 1,
    y: 2,
};

const aa: A = {
    x: 3,
    y: 4,
};

const y = sortFunc(a, aa, "x");
