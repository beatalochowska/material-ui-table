export const fakeData = `[
    {
        "id": 1,
        "status": 12,
        "health": 100,
        "view": "5.8g.tdd",
        "branchFor": "MFL_TDD_trunk",
        "svnBranch": "BTS_SC_CPLANE/branches/5.8G_TDzD",
        "correctionPolicy": "N/A",
        "correctionForms": "FL18SP_ENB_0000_05xxxx_000000: (Corr.), FL18SP_ENB_0000_05xxxx_000000: (Corr.)"  
    },
    {
        "id": 2,
        "status": 13,
        "health": 20,
        "view": "C-Plane CLANG Preparation",
        "branchFor": "MF_FDD_trunk",
        "svnBranch": "BTS_SC_CPLANE/branches/user/ftw20asn",
        "correctionPolicy": "",
        "correctionForms": "CBTS00_FSM4_9999_xxxxxx_yyyyyy: (Corr.)"
    },
    {
        "id": 3,
        "status": 11,
        "health": 80,
        "view": "4.8g.tdd",
        "branchFor": "MFL_TDD_trunk",
        "svnBranch": "",
        "correctionPolicy": "Adapted for 3GPP for the time being",
        "correctionForms": "CBTS19_FSM3_9999_xxxxxx_yyyyyy: (Corr.)"
    },
    {
        "id": 4,
        "status": 14,
        "health": 30,
        "view": "asn3gpp",
        "branchFor": "MF_FDD_trunk",
        "svnBranch": "BTS_SC_CPLANE/branches/maintenance/1903.172",
        "correctionPolicy": "all corrections are welcome :-)",
        "correctionForms": "SBTS00_ENB_9999_xxx_yyy: (Corr.)"
    },
    {
        "id": 5,
        "status": 11,
        "health": 0,
        "view": "auto_ecl_align",
        "branchFor": "MFL_TDD_trunk",
        "svnBranch": "BTS_SC_CPLANE/branches/user/MulteFire",
        "correctionPolicy": "N/A",
        "correctionForms": "SBTS00_ENB_9999_xxx_yyy: (Corr.)"
    },
    
    {
        "id":6,
        "correctionForms": "SBTS00_ENB_8888_xxx_yyy: (Corr.)",
        "parentId":5
    }
   
]
`;